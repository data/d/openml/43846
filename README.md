# OpenML dataset: 400k-NYSE-random-investments--financial-ratios

https://www.openml.org/d/43846

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This dataset was created to make the project "AI Learn to invest" for SaturdaysAI - Euskadi 1st edition. The project can be found in https://github.com/ImanolR87/AI-Learn-to-invest
Content
More than 400.000 random investments were created with the data from the last 10 years from the NYSE market. Finantial ratios and volatilities were calculated and added to the random investments dataset.
Finantial ratios included:

ESG Ranking
ROA 
ROE 
Net Yearly Income
PB
PE
PS
EPS
Sharpe 

Acknowledgements
I thank SaturdaysAI to push me falling in love with data science.
Inspiration
Our inspiration was to find an answer to why young people doesn't invest more on Stock-Exchange markets.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43846) of an [OpenML dataset](https://www.openml.org/d/43846). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43846/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43846/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43846/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

